(ns plf01.core)

(defn doble [n]
  (* 2 n))

(defn valor-absoluto [n]
  (if (pos? n)
    n
    (* n -1)))
;;< 
;;Regresa el menor de 2 numeros
(defn menor-de-2-numeros [x y]
  (if (< x y) x y))

(defn menor-de-3-numeros [x y z]
  (if (< x y z) x
      (if (< y x z) y z)))

(defn menor-a-cien [n]
  (< n 100))